#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Usage: $(basename $0) ${USER}.lockbox"
    exit 1
fi

LOCKBOX=$(readlink -f $1)

# TODO: Try all strings in $LOCKBOX as password
for password in $(strings $LOCKBOX); do
    # TODO: Exit with success if we crack $LOCKBOX using $password
    $LOCKBOX $password && exit 0
done

echo "Unable to crack $LOCKBOX!"
exit 2

# vim: set sts=4 sw=4 ts=8 ft=sh:
