#!/bin/bash

# Variables
ZIP=46556
URL="https://forecast.weather.gov/zipcity.php"
FORECAST=0
CELSIUS=0
SAVED_WEATHER_INFO=""

# Functions
usage() {
    cat 1>&2 <<EOF
Usage: $(basename $0) [zipcode]

-c    Use Celsius degrees instead of Fahrenheit for temperature
-f    Display forecast text

If zipcode is not provided, then it defaults to $ZIP.
EOF
    exit $1
}

weather_information() {
  # Fetch weather information from URL based on ZIPCODE
  SAVED_WEATHER_INFO=$(curl -sL https://forecast.weather.gov/zipcity.php?inputstring=$ZIP)
}

temperature() {
  # Extract the temperature information from weather source
  if [ $CELSIUS -ne 0 ]; then
    information=$(echo $SAVED_WEATHER_INFO | sed -n 's/.*<p class="myforecast-current-sm">\(.*\)<p.*/\1/p')
  else
    information=$(echo $SAVED_WEATHER_INFO | sed -n 's/.*<p class="myforecast-current-lrg">\(.*\)<p.*/\1/p')
  fi

  # Clean excess data from strings
  information=$(echo $information | sed 's/<\/p>.*//' | sed 's/\&deg;[FC]//g')
  
  echo $information
}

forecast() {
  # Exctract forecast information from weather source
  information=$(echo $SAVED_WEATHER_INFO | sed -n 's/.*<p class="myforecast-current">\(.*\)<p.*/\1/p')

  # Clean excess data from strings
  information=$(echo $information | sed 's/<\/p>.*//' | tr -d '[:space:]')
  echo $information
}

# Parse command line options
while [ $# -gt 0 ]; do
  case "$1" in
    -h|--help)
      usage 0
      ;;
    -c|--celsius)
      CELSIUS=1
      ;;
    -f|--forecast)
      FORECAST=1
      ;;
    [0-9][0-9][0-9][0-9][0-9]) 
      ZIP=$1
      ;;
    *)
      echo -e "\033[1;31mError:\033[m \033[1;37mInvalid usage. Please get smart by reading information below.\033[m"
      usage 0
      ;;
  esac
  shift
done

# Acquire Information
weather_information

# Display Information
if [ $FORECAST -ne 0 ]; then
  echo Forecast: "  " $(forecast)
fi
echo Temperature: $(temperature) degrees
