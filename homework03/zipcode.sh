#!/bin/bash

# Globals
URL=https://www.zipcodestogo.com/
CITY=""
STATE=""

# Functions
usage() {
  cat 1>&2 <<EOF
Usage: $(basename $0)

  -c      CITY    Which city to search
  -s      STATE   Which state to search (Indiana)

If no CITY is specified, then all the zip codes for the STATE are displayed.
EOF
    exit $1
}

get_raw_zipcode_info() {
  curl -s $URL$STATE/
}

get_zip_info() {
  get_raw_zipcode_info |  grep -Ei "/[[:alpha:][:space:]].*/[a-z]{2}/[0-9]{5}" | sed -e 's/^.*.com\(.*\)">.*$/\1/g'
}

print_all_zips() {
  get_zip_info | sed -e 's/^.*\/[A-Z][A-Z]\///g' -e 's/\/$//g'
}

print_city_zip() {
  get_zip_info | grep -i "\/$CITY\/" | sed -e 's/^.*\/[A-Z][A-Z]\///g' -e 's/\/$//g'
}

# Parse Command Line Options
if [ $# -le 1 ]; then
  STATE="Indiana"
fi

while [ $# -gt 0 ]; do
    case $1 in
    -h|--help) 
      usage 0
      ;;
    -c|--city)
      shift
      CITY=$1
      ;;
    -s|--state)
      shift
      STATE=$(echo $1 | sed 's/ /%20/g')
      ;;
     *) 
      usage 0
      ;;
    esac
    shift
done

# Filter Pipeline(s)

if [[ $CITY != '' ]]; then
  print_city_zip
else 
  print_all_zips
fi