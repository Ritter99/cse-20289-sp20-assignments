#!/usr/bin/env python3

import collections
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

import requests

# Constants

URL = 'https://yld.me/raw/axPa'
TAB = ' '*8
GENDERS = ('M', 'F')
ETHNICS = ('B', 'C', 'N', 'O', 'S', 'T', 'U')

# Functions


def usage(status=0):
    ''' Display usage information and exit with specified status '''
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [options] [URL]

    -y  YEARS   Which years to display (default: all)
    -p          Display data as percentages.
    -G          Do not include gender information.
    -E          Do not include ethnic information.
    ''')
    sys.exit(status)


def load_demo_data(url=URL):
    ''' Load demographics from specified URL into dictionary

    >>> load_demo_data('https://yld.me/raw/ilG').keys()
    dict_keys(['2013', '2014', '2015', '2016', '2017', '2018', '2019'])

    >>> load_demo_data('https://yld.me/raw/ilG')['2013']
    {'M': 1, 'B': 2, 'F': 1, 'TOTAL': 2}

    >>> load_demo_data('https://yld.me/raw/ilG')['2019']
    {'M': 1, 'U': 2, 'F': 1, 'TOTAL': 2}
    '''
    # Request data from url and load it into dictionary organized in the
    # following fashion:
    #
    #   {'year': {'gender': count, 'ethnic': count}}
    response = requests.get(url)
    data = response.text.split()
    demographic_data = {}

    # Add up total number of students for each year and store in
    # dictionary.
    for selection in data:
        selection = selection.split(',')
        ethnic = selection.pop()
        gender = selection.pop()
        year = selection.pop()

        if year != 'Year' and ethnic != 'Ethnic' and gender != 'Gender':
            try:
                try:
                    demographic_data[year][gender] += 1
                except KeyError:
                    demographic_data[year][gender] = 1
                try:
                    demographic_data[year][ethnic] += 1
                except KeyError:
                    demographic_data[year][ethnic] = 1
            except KeyError:
                demographic_data[year] = {gender: 1, ethnic: 1}

    for year in demographic_data:
        data = demographic_data[year]
        count = 0
        for gender in GENDERS:
            try:
                value = data[gender]
            except KeyError:
                value = 0
            count += value
        demographic_data[year]['TOTAL'] = count

    return demographic_data


def print_demo_separator(years, char='='):
    ''' Print demographics separator

    Note: The row consists of the 8 chars for each item in years + 1.

    >>> print_demo_separator(['2012', '2013'])
    ========================
    '''
    # Print row of separator characters
    char_str = ''
    char_str = char_str + char + char + char + char
    for i in range(len(years)):
        char_str = char_str + char + char + char + char + char + char + char + char
    char_str = char_str + char + char + char + char
    print(char_str)


def print_demo_years(years):
    ''' Print demographics years row

    Note: The row is prefixed by 4 spaces and each year is right aligned to 8
    spaces ({:>8}).

    >>> print_demo_years(['2012', '2013'])
            2012    2013
    '''
    # Print row of years
    year_str = ''
    year_str = year_str + '    '
    for year in years:
        year_str = year_str + '{:>8}'.format(year)
    print(year_str)


def print_demo_fields(data, years, fields, percent=False):
    ''' Print demographics information (for particular fields)

    Note: The first column should be a 4-spaced field name ({:>4}), followed by
    8-spaced right aligned data columns ({:>8}).  If `percent` is True, then
    display a percentage ({:>7.1f}%) rather than the raw count.

    >>> data  = load_demo_data('https://yld.me/raw/ilG')
    >>> years = sorted(data.keys())
    >>> print_demo_fields(data, years, GENDERS, False)
       M       1       1       1       1       1       1       1
       F       1       1       1       1       1       1       1
    '''
    # For each field, print out a row consisting of data from each year.
    for field in fields:
        string = ''
        string = string + '{:>4}'.format(field)
        counts = []
        for year in years:
            year_data = data[year]
            total = year_data['TOTAL']
            try:
                value = year_data[field]
            except KeyError:
                value = 0
            counts.append(value)

        for i, count in enumerate(counts, 0):
            total = data[years[i]]['TOTAL']
            if percent:
                string = string + '{:>7.1f}%'.format((count / total) * 100)
            else:
                string = string + '{:>8}'.format(count)

        print(string)


def print_demo_data(data, years=None, percent=False, gender=True, ethnic=True):
    ''' Print demographics data for the specified years and attributes '''
    # Verify the years parameter (if None then extract from data,
    # otherwise use what is given).  Ensure years is sorted.
    if years is None:
        years = list(data.keys())
    years = sorted(years)

    # Print years header with separator
    print_demo_years(years)
    print_demo_separator(years)

    # Print gender and ethic data if enabled
    if gender:
        print_demo_gender(data, years, percent)
    if ethnic:
        print_demo_ethnic(data, years, percent)


def print_demo_gender(data, years, percent=False):
    ''' Print demographics gender information '''
    print_demo_fields(data, years, GENDERS, percent)
    print_demo_separator(years, '-')


def print_demo_ethnic(data, years, percent=False):
    ''' Print demographics ethnic information '''
    print_demo_fields(data, years, ETHNICS, percent)
    print_demo_separator(years, '-')


def make_chart(data, years=None, ethnic=False):
    if ethnic:
        make_area_chart(data, years=years)
    else:
        make_bar_chart(data, years=years)


def make_bar_chart(data, years=None):
    if years is None:
        years = list(data.keys())
    years = sorted(years)
    N = len(years)
    menMeans = ()
    womenMeans = ()
    statistics = []

    for year in data:
        item = data[year]
        try:
            menPercent = int(item['M'] / item['TOTAL'] * 100)
        except ValueError:
            pass
        try:
            womenPercent = int(item['F'] / item['TOTAL'] * 100)
        except ValueError:
            pass

        statistics.append({
            'year': year,
            'men': menPercent,
            'women': womenPercent
        })

    statistics = sorted(statistics, key=lambda a: a['year'])

    for item in statistics:
        menMeans = menMeans + (item['men'],)
        womenMeans = womenMeans + (item['women'],)

    ind = np.arange(N)    # the x locations for the groups
    width = 0.8  # the width of the bars: can also be len(x) sequence

    fig = plt.figure()
    # p1 = plt.bar(ind, menMeans, width, yerr=menStd)
    p1 = plt.bar(ind, womenMeans, width)

    # p2 = plt.bar(ind, womenMeans, width,
    #              bottom=menMeans, yerr=womenStd)
    p2 = plt.bar(ind, menMeans, width,
                 bottom=womenMeans)

    plt.ylabel('Scores')
    plt.title('Scores by year and gender')
    plt.xticks(ind, years)
    plt.yticks(np.arange(0, 101, 10))
    plt.legend((p2[0], p1[0]), ('Women', 'Men'))

    fig.savefig('bar_chart.png')


def make_area_chart(data, years=None):
    y = np.array([[17, 19,  5, 16, 22, 20,  9, 31, 39,  8],
                  [46, 18, 37, 27, 29,  6,  5, 23, 22,  5],
                  [15, 46, 33, 36, 11, 13, 39, 17, 49, 17]])

    # y = np.array([])
    temp_data = []
    for year in data:
        items = data[year]
        del items['TOTAL']
        temp_data.append({
            'year': year,
            'item': items
        })

    data = sorted(temp_data, key=lambda a: a['year'])

    ethnic_dict = {}
    for ethnic in ETHNICS:
        ethnic_dict[ethnic] = []

    for parent_item in data:
        item = parent_item['item']
        for ethnic in ETHNICS:
            try:
                value = item[ethnic]
            except KeyError:
                value = 0
            ethnic_dict[ethnic].append(value)

    # print(data)
    # print(ethnic_dict)
    y = []
    for key in ethnic_dict:
        item = ethnic_dict[key]
        y.append(item)

    y = np.array(y)
    x = np.arange(10)

    # Make new array consisting of fractions of column-totals,
    # using .astype(float) to avoid integer division
    percent = y / y.sum(axis=0).astype(float) * 100

    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.stackplot(x, percent)
    ax.set_title('100 % stacked area chart')
    ax.set_ylabel('Percent (100%)')
    ax.margins(0, 0)  # Set margins to avoid "whitespace"
    ax.legend(('African American', 'Caucasian',
               'Native American / Pacific Islanders', 'Asian', 'Hispanic', 'Multiple Selection', 'Undeclared'), loc='upper center', ncol=1, bbox_to_anchor=(0.8, 0.6))

    fig.savefig('area_chart.png')


def main():
    ''' Parse command line arguments, load data from url, and then print
    demographic data. '''
    # Parse command line arguments
    arguments = sys.argv[1:]
    url = URL
    years = None
    gender = True
    ethnic = True
    percent = False

    i = 0
    while i < len(arguments):
        arg = arguments[i]
        if arg == "-h":
            usage()
        elif arg == "-y":
            i = i + 1
            try:
                years = arguments[i].split(',')
            except IndexError:
                usage()
        elif arg == "-p":
            percent = True
        elif arg == "-G":
            gender = False
        elif arg == "-E":
            ethnic = False
        elif '-'in arg:
            usage()
        else:
            url = arg
        i = i + 1

    # Load data from url and then print demograpic data with specified
    # arguments
    data = load_demo_data(url)
    # print_demo_data(data, years=years, percent=percent,
    #                 gender=gender, ethnic=ethnic)
    # make_bar_chart(data, years=years)
    # make_area_chart(data, years=years)
    make_chart(data, years=years, ethnic=ethnic)

# Main Execution


if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
