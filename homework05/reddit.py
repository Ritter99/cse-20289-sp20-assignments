#!/usr/bin/env python3

import os
import sys
import pprint

import requests

# Constants

ISGD_URL = 'http://is.gd/create.php'

# Functions


def usage(status=0):
    ''' Display usage information and exit with specified status '''
    print('''Usage: {} [options] URL_OR_SUBREDDIT

    -s          Shorten URLs using (default: False)
    -n LIMIT    Number of articles to display (default: 10)
    -o ORDERBY  Field to sort articles by (default: score)
    -t TITLELEN Truncate title to specified length (default: 60)
    '''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)


def check_url(url):
    # If URL does not contain http, assume subreddit
    if not "http" in url:
        url = 'https://reddit.com/r/' + url + '/.json'
    return url


def load_reddit_data(url):
    ''' Load reddit data from specified URL into dictionary

    >>> len(load_reddit_data('https://reddit.com/r/nba/.json'))
    27

    >>> load_reddit_data('linux')[0]['data']['subreddit']
    'linux'
    '''
    # TODO: Verify url parameter (if it starts with http, then use it,
    # otherwise assume it is just a subreddit).
    url = check_url(url)

    headers = {
        'user-agent': 'reddit-{}'.format(os.environ.get('USER', 'cse-20289-sp20'))}
    response = requests.get(url, headers=headers)
    return response.json()['data']['children']


def shorten_url(url):
    ''' Shorten URL using is.gd service

    >>> shorten_url('https://reddit.com/r/aoe2')
    'https://is.gd/dL5bBZ'

    >>> shorten_url('https://cse.nd.edu')
    'https://is.gd/3gwUc8'
    '''
    # TODO: Make request to is.gd service to generate shortened url.
    url = check_url(url)

    shortened_url = requests.get(
        ISGD_URL, params={'format': 'json', 'url': url})
    return shortened_url.json()['shorturl']


def print_reddit_data(data, limit=10, orderby='score', titlelen=60, shorten=False):
    ''' Dump reddit data based on specified attributes '''
    # TODO: Sort articles stored in data list by the orderby key, and then
    # print out each article's index, title, score, and url using the following
    # format:
    #
    #   print(f'{index:4}.\t{title} (Score: {score})\n\t{url}')
    #
    # Note: Trim or modify the output based on the keyword arguments to the function.

    # Sort the data
    reversed = False
    if orderby == 'score':
        reversed = True
    data = sorted(data, key=lambda i: i['data'][orderby], reverse=reversed)

    # Print the data
    for index, child in enumerate(data, 1):
        child_data = child['data']
        title = child_data['title'][0:titlelen]
        score = child_data['score']
        url = child_data['url']

        if shorten:
            url = shorten_url(url)

        print(f'{index:4}.\t{title} (Score: {score})\n\t{url}')

        if index == limit:
            break
        print()
    pass


def main():
    # TODO: Parse command line arguments
    arguments = sys.argv[1:]
    url = None
    limit = 10
    orderby = 'score'
    titlelen = 60
    shorten = False

    if len(arguments) <= 0:
        usage()

    # Parse Command Line Arguments
    i = 0
    while i < len(arguments):
        arg = arguments[i]
        if arg == "-h":
            usage()
        elif arg == "-s":
            shorten = True
        elif arg == "-n":
            i = i + 1
            try:
                limit = int(arguments[i])
            except IndexError:
                usage()
            except ValueError:
                usage()
        elif arg == "-o":
            i = i + 1
            try:
                orderby = arguments[i]
            except IndexError:
                usage()
        elif arg == "-t":
            i = i + 1
            try:
                titlelen = int(arguments[i])
            except IndexError:
                usage()
            except ValueError:
                usage()
        else:
            url = arg
        i = i + 1

    # TODO: Load data from url and then print the data
    data = load_reddit_data(url)
    print_reddit_data(data, limit=limit, orderby=orderby,
                      titlelen=titlelen, shorten=shorten)
    pass

# Main Execution


if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
