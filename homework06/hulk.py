#!/usr/bin/env python3

import concurrent.futures
import hashlib
import os
import string
import sys

# Constants

ALPHABET = string.ascii_lowercase + string.digits

# Functions


def usage(exit_code=0):
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [-a ALPHABET -c CORES -l LENGTH -p PATH -s HASHES]
    -a ALPHABET Alphabet to use in permutations
    -c CORES    CPU Cores to use
    -l LENGTH   Length of permutations
    -p PREFIX   Prefix for all permutations
    -s HASHES   Path of hashes file''')
    sys.exit(exit_code)


def md5sum(s):
    ''' Compute md5 digest for given string. '''
    # TODO: Use the hashlib library to produce the md5 hex digest of the given
    # string.
    return hashlib.md5(s.encode()).hexdigest()


# def permutations(length, alphabet=ALPHABET):
#     ''' Recursively yield all permutations of the given length using the
#     provided alphabet. '''
#     # TODO: Use yield to create a generator function that recursively produces
#     # all the permutations of the given length using the provided alphabet.
#     yield None

def permutations(length, alphabet=ALPHABET):
    ''' Recursively yield all permutations of the given length using the
    provided alphabet. '''
    # TODO: Use yield to create a generator function that recursively produces
    # all the permutations of the given length using the provided alphabet.

    if length <= 0:
        yield ''
    else:
        for perm in permutations(length - 1, alphabet=alphabet):
            for a in alphabet:
                yield perm + a


def flatten(sequence):
    ''' Flatten sequence of iterators. '''
    # TODO: Iterate through sequence and yield from each iterator in sequence.
    for child in sequence:
        for elem in child:
            yield elem


def crack(hashes, length, alphabet=ALPHABET, prefix=''):
    ''' Return all password permutations of specified length that are in hashes
    by sequentially trying all permutations. '''
    # TODO: Return list comprehension that iterates over a sequence of
    # candidate permutations and checks if the md5sum of each candidate is in
    # hashes.
    if prefix != '':
        length += 1

    perms = [perm for perm in permutations(
        length, alphabet=alphabet) if perm.startswith(prefix)]
    return [perm for perm in perms if md5sum(perm) in hashes]


def cracker(arguments):
    ''' Call the crack function with the specified arguments '''
    return crack(*arguments)


def smash(hashes, length, alphabet=ALPHABET, prefix='', cores=1):
    ''' Return all password permutations of specified length that are in hashes
    by concurrently subsets of permutations concurrently.
    '''
    # TODO: Create generator expression with arguments to pass to cracker and
    # then use ProcessPoolExecutor to apply cracker to all items in expression.

    arguments = []
    if prefix == '':
        arguments = ((hashes, length-1, alphabet, a)
                     for a in alphabet)
    else:
        arguments.append((hashes, length, alphabet, prefix))

    with concurrent.futures.ProcessPoolExecutor(cores) as executer:
        result = executer.map(cracker, arguments)

    result = list(flatten(result))
    return result


def main():
    arguments = sys.argv[1:]
    alphabet = ALPHABET
    cores = 1
    hashes_path = 'hashes.txt'
    length = 1
    prefix = ''

    # TODO: Parse command line arguments
    if len(arguments) <= 0:
        usage()

    i = 0
    while i < len(arguments):
        arg = arguments[i]
        if arg == "-h":
            usage()
        elif arg == "-a":
            i += 1
            alphabet = arguments[i]
        elif arg == "-c":
            i += 1
            try:
                cores = int(arguments[i])
            except IndexError:
                usage()
            except ValueError:
                usage()
        elif arg == "-l":
            i += 1
            try:
                length = int(arguments[i])
            except IndexError:
                usage()
            except ValueError:
                usage()
        elif arg == "-p":
            i += 1
            prefix = arguments[i]
        elif arg == "-s":
            i += 1
            hashes_path = arguments[i]
        # else:
        #     usage()
        i = i + 1

    # TODO: Load hashes set
    try:
        f = open(hashes_path, "r")
    except FileNotFoundError:
        exit("Error: File not found")
    if f.mode == "r":
        hashes = f.read()

    # print(hashes)

    # TODO: Execute crack or smash function
    passwords = smash(hashes, length, alphabet=alphabet,
                      prefix=prefix, cores=cores)

    # TODO: Print all found passwords
    for password in passwords:
        print(password)

# Main Execution


if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
