#!/usr/bin/env python3

import atexit
import os
import sys
import tempfile

# Functions


def usage(status=0):
    ''' Display usage message for program and exit with specified status. '''
    print(f'Usage: {os.path.basename(sys.argv[0])} files...')
    sys.exit(status)


def save_files(files):
    ''' Save list of files to a temporary file and return the name of the
    temporary file. '''

    # HINT:
    # with tempfile.NamedTemporaryFile(delete=False) as tf:
    # ...

    with tempfile.NamedTemporaryFile(delete=False, mode="w") as tf:
        for line in files:
            tf.write(line)
            tf.write('\n')
        return tf.name


def edit_files(path):
    ''' Fork a child process to edit the file specified by path using the user's
    default EDITOR (use "vim" if not set).  Parent waits for child process and
    returns whether or not the child was successful. '''

    # HINT:
    # For edit_files, you must use os.fork to create a new process.
    # To read environment variables, remember that you can access os.environ.
    # The child process should use os.execlp to execute the text editor.
    # The parent process should use os.wait to get the child's exit status.
    # To translate the process exit status, you should use os.WEXITSTATUS.

    # HINT:
    # For edit_files and move_files, you must perform error checking. In Python, you can use try/except to handle this:
    # try:
    #     pid = os.fork()
    # except OSError:
    #     ... # Handle error

    try:
        editor = os.environ['EDITOR']
    except KeyError:
        editor = 'vim'

    try:
        pid = os.fork()
    except OSError:
        return 1

    if pid == 0:  # Child
        try:
            # Edit file
            os.execlp(editor, editor, path)
        except (IndexError, OSError):
            return 1

    else:  # Parent
       # Wait for child process
        try:
            pid, status = os.wait()
        except OSError:
            return 1

        # Return exit status of child process
        return os.WEXITSTATUS(status)


def move_files(files, path):
    ''' For each file in files and the corresponding information from the file
    specified by path, rename the original source file to the new target path
    (if the names are different).  Return whether or not all files were
    successfully renamed. '''

    # HINT:
    # For move_files, consider using zip to iterate through two sequences simultaneously and use os.rename to move files.

    # HINT:
    # For edit_files and move_files, you must perform error checking. In Python, you can use try/except to handle this:
    # try:
    #     pid = os.fork()
    # except OSError:
    #     ... # Handle error

    tf = os.open(path, os.O_RDONLY)
    readBytes = os.read(tf, 1000)
    readBytes = readBytes.decode('utf-8')
    readList = readBytes.strip().split('\n')
    zipped = zip(files, readList)

    for t in zipped:
        if t[0] != t[1]:  # file name has changed, rename it
            os.rename(t[0], t[1])


def cleanup(path):
    os.unlink(path)


def main():
    ''' Parse command line arguments, save arguments to temporary file, allow
    the user to edit the temporary file, move the files, and remove the
    temporary file. '''
    # TODO: Parse command line arguments
    arguments = sys.argv[1:]
    if len(arguments) <= 0:
        usage()

    i = 0
    while i < len(arguments):
        arg = arguments[i]
        if arg == "-h":
            usage()
        else:
            files = arguments
        i += 1

    # TODO: Save files (arguments)
    path = save_files(files)

    # TODO: Register unlink to cleanup temporary file
    atexit.register(cleanup, path)

    # TODO: Edit files stored in temporary file
    failed = edit_files(path)
    if failed:
        exit("Error: Failed editing file")

    # TODO: Move files stored in temporary file
    move_files(files, path)

# Main Execution


if __name__ == '__main__':
    main()
