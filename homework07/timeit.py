#!/usr/bin/env python3

import os
import signal
import sys
import time

# Functions


def usage(status=0):
    ''' Display usage message for program and exit with specified status. '''
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [options] command...
Options:
    -t SECONDS  Timeout duration before killing command (default is 10)
''')
    sys.exit(status)


def error(message, status=1):
    ''' Display error message and exit with specified status. '''
    print(message, file=sys.stderr)
    sys.exit(status)


def alarm_handler(signum, frame):
    ''' Alarm handler that raises InterruptedError '''

    # HINT:
    # For alarm_handler, you must raise an InterruptedError.
    raise TimeoutError("Command took too long to execute")


def timeit(argv, timeout=10):
    ''' Run command specified by argv for at most timeout seconds.

    - After forking, the child executes the specified command.

    - After forking, the parent does the following:
        - Registers a handler for SIGALRM
        - Set alarm for specified timeout
        - Waits for child process
        - If wait is interrupted, kills the child process and wait again
        - Prints total elapsed time
        - Exits with the status of the child process
    '''

    # HINT:
    # For timeit, you must use os.fork to create a new process.
    # * The child process should use os.execvp to execute the command.
    # * The parent process should use signal.signal to register a signal handler for signal.SIGALRM.
    # * The parent process should use signal.alarm to set an alarm.
    # * The parent process should use os.wait to wait for the child process.
    # * Note: A InterruptedError exception is raised by the alarm_handler if the signal.alarm is triggered.
    # * The parent process should use os.kill with signal.SIGKILL to terminate the child process if it exceeds its time limit.
    # * Note: A ProcessLookupError is raised if you call os.kill on child process that is no longer running.
    # * The parent process should use time.time to keep track of timestamps in order to compute elapsed time.
    # * The parent process should use os.WEXITSTATUS, os.WIFEXITED, and os.WTERMSIG to determine the actual exit status of the child process.
    # * Note: If the child process was killed by the parent process, then the status should be the signal used to kill the * child process.

    startTime = time.time()
    try:
        pid = os.fork()
    except OSError:
        return 1

    if pid == 0:  # Child
        try:
            # Edit file
            os.execvp(argv[0], argv)
        except (IndexError, OSError):
            return 1

    else:  # Parent

        # Wait for child process
        try:
            sig = signal.signal(signal.SIGALRM, alarm_handler)
            ala = signal.alarm(timeout)
            pid, status = os.wait()
        except InterruptedError:
            try:
                os.kill(0, signal.SIGKILL)
            except ProcessLookupError:
                pass
        except OSError:
            currentTime = time.time()
            elapsed = round(currentTime - startTime, 3)
            print(f'Time Elapsed: {elapsed:.2f}')
            sys.exit(9)
            return 1

        # Return exit status of child process
        # print(os.WEXITSTATUS(status), os.WIFEXITED(status), os.WTERMSIG(status))
        currentTime = time.time()
        elapsed = round(currentTime - startTime, 3)
        print(f'Time Elapsed: {elapsed:.2f}')
        sys.exit(os.WEXITSTATUS(status))

    pass


def main():
    ''' Parse command line options and then execute timeit with specified
    command and timeout. '''

    arguments = sys.argv[1:]
    if len(arguments) <= 0:
        usage(status=1)

    i = 0
    command = 0
    timeout = 10
    while i < len(arguments):
        arg = arguments[i]
        if arg == "-h":
            usage()
        elif arg == "-t":
            i += 1
            try:
                timeout = int(arguments[i])
            except ValueError:
                exit(
                    "Error: -t must be followed by an int\n\nCheck usage for help:\ntimeit.py -h")
        else:
            command = arguments[i-1:]
        i += 1

    if not command:
        usage(status=2)

    timeit(command, timeout=timeout)

# Main Execution


if __name__ == '__main__':
    main()
