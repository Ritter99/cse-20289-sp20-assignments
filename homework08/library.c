/* library.c: string utilities library */

#include "str.h"

#include <ctype.h>
#include <string.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 **/
void str_lower(char *s)
{
    for (char *c = s; *c; c++)
    {
        *(c) = tolower(*c);
    }
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 **/
void str_upper(char *s)
{
    for (char *c = s; *c; c++)
    {
        *(c) = toupper(*c);
    }
}

/**
 * Convert all characters in string to titlecase.
 * @param   s       String to convert
 **/
void str_title(char *s)
{
    for (char *c = s; *c; c++)
    {
        *c = tolower(*c);
        if (!isalpha(*(c - 1)))
        {
            *(c) = toupper(*c);
        }
    }
}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 **/
void str_chomp(char *s)
{
    char *tail = s + strlen(s) - 1;
    if (*(tail) == '\n')
    {
        *(tail) = '\0';
    }
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 **/
void str_strip(char *s)
{
    int i = 0;
    char *w = s;
    bool beginning = true;
    // bool finished = false;
    for (char *c = s; *c; c++)
    {
        if (*c == ' ' && beginning) // whitespace at front, ignore
        {
        }
        else if (*c != ' ' && beginning) // first non-whitespace character
        {
            *(w + i) = *c;
            i++;
            beginning = false;
        }
        // else if (!beginning && *c == ' ' && *(c + 1) == ' ')
        // {
        //     *(w + i) = '\0';
        //     break;
        // }
        else if (!beginning && *c == ' ' && *(c + 1) == '\0')
        {
            *(w + i) = '\0';
            break;
        }
        else if (!beginning) // not at front
        {
            *(w + i) = *c;
            i++;
        }
    }

    if (*(w + i) != '\0')
    {
        *(w + i) = '\0';
    }
    if (*(w + i - 1) == ' ')
    {
        *(w + i - 1) = '\0';
    }
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 **/
void str_reverse(char *s)
{
    if (*(s) == '\0')
    {
        return;
    }
    char *center = s + (int)(strlen(s) / 2);
    for (char *c = s; *c; c++)
    {
        char *tail = s + strlen(c) - 1;
        char tmp = *tail;
        *tail = *c;
        *c = tmp;
        if (tail == center)
        {
            break;
        }
    }
}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 **/
void str_translate(char *s, char *from, char *to)
{
    if (strlen(from) != strlen(to))
    {
        return;
    }
    for (char *c = s; *c; c++)
    {
        int i = 0;
        for (char *f = from; *f; f++)
        {
            if (*c == *f)
            {
                *c = *(to + i);
            }
            i++;
        }
    }
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int str_to_int(const char *s, int base)
{

    long int returnN = 0;

    for (size_t i = 0; i < strlen(s); i++)
    {
        int n;
        switch (s[i])
        {
        case '0':
            n = 0;
            break;
        case '1':
            n = 1;
            break;
        case '2':
            n = 2;
            break;
        case '3':
            n = 3;
            break;
        case '4':
            n = 4;
            break;
        case '5':
            n = 5;
            break;
        case '6':
            n = 6;
            break;
        case '7':
            n = 7;
            break;
        case '8':
            n = 8;
            break;
        case '9':
            n = 9;
            break;
        case 'A':
            n = 10;
            break;
        case 'a':
            n = 10;
            break;
        case 'B':
            n = 11;
            break;
        case 'b':
            n = 11;
            break;
        case 'C':
            n = 12;
            break;
        case 'c':
            n = 12;
            break;
        case 'D':
            n = 13;
            break;
        case 'd':
            n = 13;
            break;
        case 'E':
            n = 14;
            break;
        case 'e':
            n = 14;
            break;
        case 'F':
            n = 15;
            break;
        case 'f':
            n = 15;
            break;
        }

        // return n;

        int gazingalingling = 1;

        for (int j = 0; j < (strlen(s) - i - 1); j++)
        {
            gazingalingling *= base;
        }

        returnN += n * gazingalingling;
    }

    return returnN;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
