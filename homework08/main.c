/* main.c: string library utility */

#include "str.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = "./str-static";

/* Flags */

enum
{
    /* Enumerate Flags */
    STRIP = 1 << 1,
    REVERSE = 1 << 2,
    LOWER = 1 << 3,
    UPPER = 1 << 4,
    TITLE = 1 << 5,
};

/* Functions */

void usage(int status)
{
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s      Strip whitespace\n");
    fprintf(stderr, "   -r      Reverse line\n");
    fprintf(stderr, "   -l      Convert to lowercase\n");
    fprintf(stderr, "   -u      Convert to uppercase\n");
    fprintf(stderr, "   -t      Convert to titlecase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int flag)
{
    /* TODO: Process each line in stream by performing transformations */
    char *chunk[128];
    while (fgets(chunk, sizeof(chunk), stream) != NULL)
    {

        if (source != NULL && target != NULL)
        {
            str_translate(chunk, source, target);
        }

        if (flag & STRIP)
        {
            str_strip(chunk);
        }

        if (flag & REVERSE)
        {
            str_reverse(chunk);
        }

        if (flag & LOWER)
        {
            str_lower(chunk);
        }

        if (flag & UPPER)
        {
            str_upper(chunk);
        }

        if (flag & TITLE)
        {
            str_title(chunk);
        }

        fprintf(stdout, chunk);
    }
}

/* Main Execution */

int main(int argc, char *argv[])
{
    int argind = 1;

    /* TODO: Parse command line arguments */
    PROGRAM_NAME = argv[0]; /* 5 */
    char *source = NULL;
    char *target = NULL;
    int flag = 0;
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-')
    { /* 6 */
        char *arg = argv[argind++];
        // fprintf(stdout, arg);
        // fprintf(stdout, "\n");
        switch (arg[1])
        {
        case 'h':
            usage(0);
            break;
        case 's':
            flag |= STRIP;
            break;
        case 'r':
            flag |= REVERSE;
            break;
        case 'l':
            flag |= LOWER;
            break;
        case 'u':
            flag |= UPPER;
            break;
        case 't':
            flag |= TITLE;
            break;
        }
    }

    // argind = 1;
    while (argind < argc && strlen(argv[argind]) > 0 && argv[argind][0] != '-')
    {
        char *arg = argv[argind++];
        if (source == NULL)
        {
            source = arg;
        }
        else if (target == NULL)
        {
            target = arg;
        }
    }

    // fprintf(stdout, source);
    // fprintf(stdout, target);

    // if (source == NULL || target == NULL)
    // {
    //     usage(1);
    // }

    /* TODO: Translate Stream */
    translate_stream(stdin, source, target, flag);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
