/* duplicates.c */

#include "hash.h"
#include "macros.h"
#include "table.h"

#include <dirent.h>
#include <limits.h>
#include <sys/stat.h>

/* Globals */
char *PROGRAM_NAME = NULL;

/* Structures */

typedef struct
{
    bool count;
    bool quiet;
} Options;

/* Functions */

void usage(int status)
{
    fprintf(stderr, "Usage: %s paths...\n", PROGRAM_NAME);
    fprintf(stderr, "    -c     Only display total number of duplicates\n");
    fprintf(stderr, "    -q     Do not write anything (exit with 0 if duplicate found)\n");
    exit(status);
}

// used reference: https://stackoverflow.com/questions/4553012/checking-if-a-file-is-a-directory-or-just-a-file
/**
 * Check if path is a directory.
 * @param       path        Path to check.
 * @return      true if Path is a directory, otherwise false.
 */
bool is_directory(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISDIR(path_stat.st_mode);
}

/**
 * Check if file is in table of checksums.
 *
 *  If quiet is true, then exit if file is in checksums table.
 *
 *  If count is false, then print duplicate association if file is in
 *  checksums table.
 *
 * @param       path        Path to file to check.
 * @param       checksums   Table of checksums.
 * @param       options     Options.
 * @return      0 if Path is not in checksums, otherwise 1.
 */
size_t check_file(const char *path, Table *checksums, const Options *options)
{
    /* Create hash of the file. Check if it exists in checksums */
    char *key = (char *)calloc(HEX_DIGEST_LENGTH, sizeof(char));
    if (!hash_from_file(path, key))
    {
        exit(1);
    }
    Value *v = table_search(checksums, key);
    size_t tr = 0; // Value to return

    if (v == NULL)
    { /* File is not in checksums. Add it */
        Value val;
        Type t = STRING;
        val.string = (char *)(path);
        table_insert(checksums, key, val, t);
    }
    else
    { /* file already exists in checksums (aka it is a duplicate) */
        tr = 1;
        if (options->quiet)
        { // quiet option set, exit now with code zero
            exit(0);
        }
        if (!options->count)
        { // count option not set, inform user of duplicate file
            printf("%s is duplicate of %s\n", path, v->string);
        }
    }
    free(key);
    return tr;
}

/**
 * Check all entries in directory (recursively).
 * @param       root        Path to directory to check.
 * @param       checksums   Table of checksums.
 * @param       options     Options.
 * @return      Number of duplicate entries in directory.
 */
size_t check_directory(const char *root, Table *checksums, const Options *options)
{
    /* Open the root directory for reading */
    if (root == NULL)
    {
        return 0;
    }
    DIR *d;
    struct dirent *dir;
    d = opendir(root);
    int count = 0;

    /* Check each file in the directory */
    if (d)
    { // file exists
        while ((dir = readdir(d)) != NULL)
        { // perform actions on each file in the directory
            char *path;
            size_t len = strlen(root) + strlen(dir->d_name) + 1;
            path = malloc(len + 1);
            ((char *)path)[len] = 0;
            strcpy(path, root);
            strcat(path, "/");
            strcat(path, dir->d_name);

            if (streq(dir->d_name, ".") || streq(dir->d_name, ".."))
            { // need to ignore . and .. otherwise we will recurse endlessly
            }
            else if (is_directory(path))
            { // if file is a directory, recursively search its files
                count += check_directory(path, checksums, options);
            }
            else
            { // file is not a directory, check if it is in checksums
                count += check_file(path, checksums, options);
            }
            free(path);
        }
        closedir(d);
    }

    return count;
}

/**
 * Initialize all options to be false
 * @param   options     Options
 * @return              NULL 
 */
void init_options(Options *options)
{
    options->count = false;
    options->quiet = false;
}

/* Main Execution */
/**
 * Main function of the program 
 * @param   argc    Number of arguments being input 
 * @param   argv    Actual input arguments 
 */
int main(int argc, char *argv[])
{
    /* Parse command line arguments */
    PROGRAM_NAME = argv[0];
    int argind = 1;
    Options options;
    init_options(&options);
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-')
    {
        char *arg = argv[argind++];
        switch (arg[1])
        {
        case 'h':
            usage(1);
            break;
        case 'c':
            options.count = true;
            break;
        case 'q':
            options.quiet = true;
            break;
        default:
            usage(1);
        }
    }

    /* Create Table for checksums */
    Table *checksums = table_create(0);
    size_t count = 0;

    /* Check each argument */
    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] != '-')
    {
        char *arg = argv[argind++];
        // debug("%s", arg);
        if (is_directory(arg))
        { // path points to a directory
            count += check_directory(arg, checksums, &options);
        }
        else
        { // path points to a file (presumably)
            count += check_file(arg, checksums, &options);
        }
    }

    /* Display count if necessary */
    if (options.count)
    {
        printf("%d\n", count);
    }

    table_delete(checksums);

    if (options.quiet)
    {
        return EXIT_FAILURE;
    }
    else
    {
        return EXIT_SUCCESS;
    }
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
