/* hash.c: hash functions */

#include "hash.h"

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

/**
 * Constants
 * http://isthe.com/chongo/tech/comp/fnv/
 */

#define FNV_OFFSET_BASIS (0xcbf29ce484222325ULL)
#define FNV_PRIME (0x100000001b3ULL)

/**
 * Compute FNV-1 hash.
 * @param   data            Data to hash.
 * @param   n               Number of bytes in data.
 * @return  Computed hash as 64-bit unsigned integer.
 */
uint64_t hash_from_data(const void *data, size_t n)
{
    uint64_t hash = FNV_OFFSET_BASIS;
    char *c_data = (char *)data;
    for (size_t i = 0; i < n; i++)
    {
        char byte = c_data[i];
        hash = hash ^ byte;
        hash = hash * FNV_PRIME;
    }
    return hash;
}

/**
 * Compute MD5 digest.
 * @param   path            Path to file to checksum.
 * @param   hexdigest       Where to store the computed checksum in hexadecimal.
 * @return  Whether or not the computation was successful.
 */
bool hash_from_file(const char *path, char hexdigest[HEX_DIGEST_LENGTH])
{
    int p = open(path, O_RDONLY);

    int n = 0;
    MD5_CTX c;
    char buf[256];
    ssize_t bytes;
    unsigned char out[HEX_DIGEST_LENGTH];

    for (n = 0; n < HEX_DIGEST_LENGTH; n++)
    {
        out[n] = '\0';
    }

    MD5_Init(&c);
    bytes = read(p, buf, 256);
    while (bytes > 0)
    {
        MD5_Update(&c, buf, bytes);
        bytes = read(p, buf, 256);
    }

    close(p);
    MD5_Final(out, &c);

    if (out == NULL)
    {
        return false;
    }

    for (n = 0; n < HEX_DIGEST_LENGTH; n++)
    {
        if (out[n] == '\0')
            break;
        sprintf(((char *)hexdigest) + 2 * n, "%02x", out[n]);
    }

    return true;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
