/* table.c: Separate Chaining Hash Table */

#include "table.h"
#include "hash.h"
#include "macros.h"

/**
 * Create Table data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @return  Allocated Table structure.
 */
Table *table_create(size_t capacity)
{
    Table *t = (Table *)calloc(1, sizeof(Table));
    if (t == NULL)
        return NULL;

    t->capacity = DEFAULT_CAPACITY;
    if (capacity > 0)
        t->capacity = capacity;
    t->size = 0;
    t->buckets = (Pair *)calloc(t->capacity, sizeof(Pair));

    return t;
}

/**
 * Delete Table data structure.
 * @param   t               Table data structure.
 * @return  NULL.
 */
Table *table_delete(Table *t)
{
    for (int i = 0; i < t->capacity; i++)
    {
        pair_delete(t->buckets[i].next, true);
    }
    free(t->buckets);
    free(t);
    return NULL;
}

/**
 * Insert or update Pair into Table data structure.
 * @param   t               Table data structure.
 * @param   key             Pair's key.
 * @param   value           Pair's value.
 * @param   type            Pair's value's type.
 */
void table_insert(Table *t, const char *key, const Value value, Type type)
{
    if (t == NULL || t->buckets == NULL)
        return;

    uint64_t hashBucket = hash_from_data(key, strlen(key)) % t->capacity;

    for (Pair *p = t->buckets[hashBucket].next; p != NULL; p = p->next)
    {
        if (streq(p->key, key))
        { // Pair with given key already exists, update and exit
            pair_update(p, value, type);
            return;
        }
    }

    // Pair with given key doesn't exist in this table, create new
    t->buckets[hashBucket].next = pair_create(key, value, t->buckets[hashBucket].next, type);
    t->size++;
}

/**
 * Search Table data structure by key.
 * @param   t               Table data structure.
 * @param   key             Key of the Pair to search for.
 * @return  Pointer to the Value associated with specified key (or NULL if not found).
 */
Value *table_search(Table *t, const char *key)
{
    if (t == NULL || t->buckets == NULL || key == NULL || strlen(key) < 1)
    {
        return NULL;
    }

    uint64_t hashBucket = hash_from_data(key, strlen(key)) % t->capacity;

    for (Pair *p = t->buckets[hashBucket].next; p != NULL; p = p->next)
    {
        // debug("inside the loop");
        if (streq(p->key, key))
        {
            // debug("Inside if statement");
            return &p->value;
        }
    }

    return NULL;
}

/**
 * Remove Pair from Table data structure with specified key.
 * @param   t               Table data structure.
 * @param   key             Key of the Pair to remove.
 * return   Whether or not the removal was successful.
 */
bool table_remove(Table *t, const char *key)
{
    if (t == NULL || t->buckets == NULL)
    {
        return false;
    }

    uint64_t housingBucket = hash_from_data(key, strlen(key)) % (uint64_t)t->capacity;
    Pair *prePair = &t->buckets[housingBucket];

    for (Pair *curPair = t->buckets[housingBucket].next; curPair != NULL; curPair = curPair->next)
    {
        if (streq(curPair->key, key))
        {
            prePair->next = curPair->next;
            pair_delete(curPair, false);
            t->size--;
            return true;
        }
        prePair = curPair;
    }

    return false;
}

/**
 * Format all the entries in the Table data structure.
 * @param   m               Table data structure.
 * @param   stream          File stream to write to.
 */
void table_format(Table *t, FILE *stream)
{
    for (int i = 0; i < t->capacity; i++)
    { // Iterate through every bucket
        for (Pair *p = t->buckets[i].next; p != NULL; p = p->next)
        { // Iterate through every bucket's pair
            pair_format(p, stream);
        }
    }
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
