#!/bin/bash

q1_answer() {
    # TODO: Complete pipeline
    echo "All your base are belong to us" | tr '[:lower:]' '[:upper:]'  
}

q2_answer() {
    # TODO: Complete pipeline
    echo "monkeys love bananas" | sed -e "s/monkeys/gorillaz/g"
}

q3_answer() {
    # TODO: Complete pipeline
    echo "     monkeys love bananas" | awk '{$1=$1};1'
}

q4_answer() {
    # TODO: Complete pipeline
    curl -sL https://yld.me/raw/yWh | awk -F ":" '/root:\/bin/ {print $7}'
}

q5_answer() {
    # TODO: Complete pipeline
    curl -sL https://yld.me/raw/yWh | sed -e "s/\/bin\/bash/\/usr\/bin\/python/g" | sed -e "s/\/bin\/csh/\/usr\/bin\/python/g" | sed -e "s/\/bin\/tcsh/\/usr\/bin\/python/g" | grep python
}

q6_answer() {
    # TODO: Complete pipeline
    curl -sL https://yld.me/raw/yWh | grep -E '4[0-9]{0,10}7' 
}
