#!/bin/bash

q1_answer() {
    # TODO: List only the names of the turtles in sorted order.
    curl -sL https://yld.me/raw/lE8 | cut -d ":" -f 1 | sort
}

q2_answer() {
    # TODO: List only the colors of the turtles in all capitals.
    curl -sL https://yld.me/raw/lE8 | cut -d ":" -f 2 | tr [a-z] [A-Z]
}

q3_answer() {
    # TODO: Replace all weapons with plowshares
    curl -sL https://yld.me/raw/lE8 | awk -F ":" '{ gsub(".*.","plowshare",$3); print $1":"$2":"$3 }'
}

q4_answer() {
    # TODO: List only the turtles whose names end in lo.
    curl -sL https://yld.me/raw/lE8 | cut -d ":" -f 1 | grep ".*lo$"
}

q5_answer() {
    # TODO: List only the turtles with names that have two consecutive vowels.
    curl -sL https://yld.me/raw/lE8 | cut -d ":" -f 1 | grep ".*[aeiou][aeiou].*"
}

q6_answer() {
    # TODO: Count how many colors don't begin with a vowel
    # WHY MUST WE REMOVE THE LEADING AND TRAILING SPACES TO GET FULL CREDIT?!?!?!
    curl -sL https://yld.me/raw/lE8 | cut -d ":" -f 2 | grep "^[^aeiou]" | wc -w | sed -e 's/^[[:space:]]*//'
}

q7_answer() {
    # TODO: List only the turtles names whose name ends with a vowel and whose weapon ends with a vowel.
    curl -sL https://yld.me/raw/lE8 | grep ".*[aeiou^:]:.*$" | grep ".*[aeiou]$" | cut -d ":" -f 1
}

q8_answer() {
    # TODO: List only the turtles names with two of the same consecutive letter (i.e. aa, bb, etc.)
    curl -sL https://yld.me/raw/lE8 | grep "^.*\([a-z]\)\1.*:[^:].*" | cut -d ":" -f 2
}
