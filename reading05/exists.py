#!/usr/bin/env python3

import os
import sys

for file in sys.argv:
    if os.path.isfile(file):
        print(f"{file} exists!")
    else:
        print(f"{file} does not exist!")
        sys.exit(1)

sys.exit(0)
