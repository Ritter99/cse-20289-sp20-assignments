#!/usr/bin/env python3

import collections
import re
import requests

# Constants

URL = 'https://cse.nd.edu/people/faculty'

# Initialize a dictionary with integer values
counts = collections.defaultdict(int)

# TODO: Make a HTTP request to URL
response = requests.get(URL)

# TODO: Access text from response object
data = response.text

# TODO: Compile regular expression to extract degrees and years of each faculty
# member
# s|.*<em>([PhD\.MS]+).*([0-9]{4}).*</em>.*|\2|p
regex = "s|.*<em>([PhD\.MS]+).*([0-9]{4}).*</em>.*|\2|p"

# TODO: Search through data using compiled regular expression and count up all
# the faculty members per year.
counts = collections.defaultdict(int)
for fa in re.findall(regex, data):
    if fa[1] != '':
        counts[fa[1]] = counts[fa[1]] + 1

# TODO: Sort items in counts by key in reverse order
# MAPPING = [
#     ('a', '4'),
#     ('o', '0'),
#     ('i', '1'),
#     ('e', '3'),
#     ('s', '5'),
# ]
items = []
for date in counts:
    count = counts[date]
    items.append((date, "      " + str(count)))
items = sorted(items, key=lambda p: p[0], reverse=True)
# print(items)

# Sort items by value in reverse order and display counts and years
for year, count in sorted(items, key=lambda p: p[1], reverse=True):
    print(count, year)
