#!/usr/bin/env python3
import csv

# Constants

PATH = '/etc/passwd'

# TODO: Loop through ':' delimited data in PATH and extract the fifth field
# (user description)
descriptions = []
with open(PATH, newline='') as csvfile:
    inread = csv.reader(csvfile, delimiter=':')
    for row in inread:
        # print(row)
        if len(row) >= 5:
            # print(row[4])
            if row[4]:
                descriptions.append(row[4])

# TODO: Print user descriptions in sorted order
descriptions = sorted(descriptions)
for description in descriptions:
    print(description)
pass
