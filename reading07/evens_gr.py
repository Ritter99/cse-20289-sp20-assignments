#!/usr/bin/env python3

import sys


def evens(stream):
    for number in stream:
        if int(number) % 2 == 0:
            yield number.strip()


print(' '.join(evens(sys.stdin)))
