#!/usr/bin/env python3

import os
import sys

# HINTS
# Use os.fork to create a new process.
# Use os.execvp to change the code of the current process.
# Use try/except to handle OSErrors.
# Use sys.exit with a status of 2 if there child fails to execute.
# Use os.WEXITSTATUS to process the status from [os.wait] in the parent


def doit(argv):
    try:
        pid = os.fork()    # TODO: Create new process
    except OSError:
        return 1

    if pid == 0:      # Child
        try:
            # TODO: Execute new code in current process
            os.execvp(argv[0], argv)
        except (IndexError, OSError):
            # TODO: Exit with status 2
            sys.exit(2)

    else:             # Parent
        # TODO: Wait for child process
        try:
            pid, status = os.wait()
        except OSError:
            pass

        # TODO: Return exit status of child process
        return os.WEXITSTATUS(status)


if __name__ == '__main__':
    status = doit(sys.argv[1:])
    sys.exit(status)
