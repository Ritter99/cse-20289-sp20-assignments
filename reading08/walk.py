#!/usr/bin/env python3

import os
import sys

# Use os.scandir to iterate through directory
# Use sorted with custom key to order by name
# Use try/except to handle OSErrors

# TODO: Determine which directory to walk from command line argument
args = sys.argv[1:]
if args:
    directory = args[0]
else:
    directory = None

# TODO: Walk specified directory in sorted order and print out each entry's
# file name
if directory is None:
    files = os.scandir()
else:
    files = os.scandir(directory)

files = sorted(files, key=lambda a: a.name)
for f in files:
    print(f.name)
