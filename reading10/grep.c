/* cat.c */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;

/* Functions */

void usage(int status)
{
  fprintf(stderr, "Usage: %s\n", PROGRAM_NAME); /* 1 */
  exit(status);                                 /* 2 */
}

bool cat_stream(FILE *stream, const char *needle)
{
  char buffer[BUFSIZ];
  bool toReturn = false;

  while (fgets(buffer, BUFSIZ, stream))
  {
    char *occurance = strstr(buffer, needle);
    if (occurance)
    {
      fputs(buffer, stdout);
      toReturn = true;
    }
  }

  return toReturn;
}

/* Main Execution */

int main(int argc, char *argv[])
{
  int argind = 1;

  // fputs(argc, stdout);
  if (argc < 2)
  {
    usage(1);
  }

  /* Parse command line arguments */
  PROGRAM_NAME = argv[0]; /* 5 */
  while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-')
  { /* 6 */
    char *arg = argv[argind++];
    switch (arg[1])
    {
    case 'h':
      usage(0);
      break;
    default:
      usage(1);
      break;
    }
  }

  /* Process each file */
  return !cat_stream(stdin, argv[1]);
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */